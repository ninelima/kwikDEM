# kwikEFIS
Digital Elevation data for Kwik EFIS.

<a href="https://search.f-droid.org/?q=kwik">
    <img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
    alt="Get it on F-Droid"
    height="80">
</a>

Instructions: https://ninelima.org/efis/instructions.html

The author wishes to acknowledge the following people and organizations:

United States Geological Survey (https://www.usgs.gov)
U.S. Geological Survey <a href="https://www.usgs.gov">USGS</a> and 
<a href="http://www.webgis.com/srtm30.html"> Web GIS</a>; 
For the data they so kindly make freely available.</li>

