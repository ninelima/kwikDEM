Changelog
=========

DEM_1.5 (2021-06-01)
------------------
* Move all DEM to independent repository

REL_45 (2021-05-01)
------------------
* Release Antarctica / South Pole DEM (player.efis.data.ant.spl)

REL_39 (2020-12-12)
------------------
* Add Greenland to eur.rus DEM
* Add Indian ocean islands to zar.aus DEM
* Add Pacific ocean islands to usa.can DEM