#!/bin/bash

pskill java
#pskill adb

  rm ./apk/kwik-efis-datapac-zar.aus.apk
  rm ./apk/kwik-efis-datapac-usa.can.apk
  rm ./apk/kwik-efis-datapac-eur.rus.apk
  rm ./apk/kwik-efis-datapac-sah.jap.apk
  rm ./apk/kwik-efis-datapac-pan.arg.apk
  rm ./apk/kwik-efis-datapac-ant.spl.apk

cat ./settings.gradle
./gradlew clean

#./gradlew build
./gradlew assemble
#./gradlew assembleDebug


cp ./datapac/build/outputs/apk/ant_spl/debug/datapac-ant_spl-debug.apk ./apk/kwik-efis-datapac-ant.spl.apk
cp ./datapac/build/outputs/apk/eur_rus/debug/datapac-eur_rus-debug.apk ./apk/kwik-efis-datapac-eur.rus.apk
cp ./datapac/build/outputs/apk/pan_arg/debug/datapac-pan_arg-debug.apk ./apk/kwik-efis-datapac-pan.arg.apk
cp ./datapac/build/outputs/apk/sah_jap/debug/datapac-sah_jap-debug.apk ./apk/kwik-efis-datapac-sah.jap.apk
cp ./datapac/build/outputs/apk/usa_can/debug/datapac-usa_can-debug.apk ./apk/kwik-efis-datapac-usa.can.apk
cp ./datapac/build/outputs/apk/zar_aus/debug/datapac-zar_aus-debug.apk ./apk/kwik-efis-datapac-zar.aus.apk

cp ./CHANGELOG.md ./apk/CHANGELOG.md

pskill java
#exit

# Copy to Sync 

cp ./datapac/build/outputs/apk/ant_spl/debug/datapac-ant_spl-debug.apk v:/Sync/Public/KwikEFIS/kwik-efis-datapac-ant.spl.apk &
cp ./datapac/build/outputs/apk/eur_rus/debug/datapac-eur_rus-debug.apk v:/Sync/Public/KwikEFIS/kwik-efis-datapac-eur.rus.apk &
cp ./datapac/build/outputs/apk/pan_arg/debug/datapac-pan_arg-debug.apk v:/Sync/Public/KwikEFIS/kwik-efis-datapac-pan.arg.apk &
cp ./datapac/build/outputs/apk/sah_jap/debug/datapac-sah_jap-debug.apk v:/Sync/Public/KwikEFIS/kwik-efis-datapac-sah.jap.apk &
cp ./datapac/build/outputs/apk/usa_can/debug/datapac-usa_can-debug.apk v:/Sync/Public/KwikEFIS/kwik-efis-datapac-usa.can.apk &
cp ./datapac/build/outputs/apk/zar_aus/debug/datapac-zar_aus-debug.apk v:/Sync/Public/KwikEFIS/kwik-efis-datapac-zar.aus.apk &
cp ./CHANGELOG.md v:/Sync/Public/KwikEFIS/CHANGELOG.md

pskill java

exit


